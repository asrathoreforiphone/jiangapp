//
//  IngredientObject+CoreDataProperties.swift
//  Jiang
//
//  Created by Nidhi Singh Naruka on 16/10/20.
//  Copyright © 2020 htmchu. All rights reserved.
//
//

import Foundation
import CoreData


extension IngredientObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<IngredientObject> {
        return NSFetchRequest<IngredientObject>(entityName: "IngredientObject")
    }

    @NSManaged public var name: String?
    @NSManaged public var isSelected: Bool
    @NSManaged public var image: Data?

}
