//
//  RecipeObject+CoreDataProperties.swift
//  Jiang
//
//  Created by Nidhi Singh Naruka on 16/10/20.
//  Copyright © 2020 htmchu. All rights reserved.
//
//

import Foundation
import CoreData


extension RecipeObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<RecipeObject> {
        return NSFetchRequest<RecipeObject>(entityName: "RecipeObject")
    }

    @NSManaged public var name: String?
    @NSManaged public var notes: String?
    @NSManaged public var ingredients: NSSet?

}

// MARK: Generated accessors for ingredients
extension RecipeObject {

    @objc(addIngredientsObject:)
    @NSManaged public func addToIngredients(_ value: IngredientObject)

    @objc(removeIngredientsObject:)
    @NSManaged public func removeFromIngredients(_ value: IngredientObject)

    @objc(addIngredients:)
    @NSManaged public func addToIngredients(_ values: NSSet)

    @objc(removeIngredients:)
    @NSManaged public func removeFromIngredients(_ values: NSSet)

}
