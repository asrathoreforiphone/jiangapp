//
//  ViewControllerExtenssion.swift
//  Jiang
//

import Foundation
import UIKit
extension UIViewController {
    
    func setMyNavigationBarUI(title:String = "Title"){
        self.title = title
        self.navigationController?.navigationBar.barTintColor = appNavBarColor
        self.navigationController?.navigationBar.tintColor = .black
        self.view.backgroundColor = vcBgColor
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    
       func showAlert(message: String, title: String = "") {
           let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
           let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
           alertController.addAction(OKAction)
           self.present(alertController, animated: true, completion: nil)
       }   

}
