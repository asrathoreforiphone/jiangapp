//
//  Application.swift
//  Jiang
//
//  Created by Nidhi Singh Naruka on 15/10/20.
//  Copyright © 2020 htmchu. All rights reserved.
//

import Foundation
import UIKit

class Application {
    static var shared:Application = Application()
    
    func setRootVCAsRecipeList(){
        let storyboard = UIStoryboard(name: "Recipe", bundle: nil)
        let recipeListNav = storyboard.instantiateViewController(withIdentifier: "RecipeListNav") as! UINavigationController
        
        
        if #available(iOS 13, *) {
            // do nothing / do things that should only be done for iOS 13
            sceneDelegate?.window?.rootViewController = recipeListNav
            sceneDelegate?.window?.makeKeyAndVisible()
        } else {
            // do iOS 12 specific window setup
            applicationDelegate?.window?.rootViewController =  recipeListNav
            applicationDelegate?.window?.makeKeyAndVisible()
        }
    }

    
 
    
}
