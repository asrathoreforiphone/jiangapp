//
//  DBManager.swift
//  Jiang
//

import Foundation

import Foundation
import UIKit
import CoreData

class DBManager {
    
    
    static var shared:DBManager = DBManager()
    
    
    func saveRecipe(recipe:Recipe){
        // 1
        let managedContext = applicationDelegate?.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "RecipeObject", in: managedContext ?? NSManagedObjectContext() )!
        
        let recipeObject = NSManagedObject(entity: entity, insertInto: managedContext) as! RecipeObject
        // 3
        recipeObject.setValue((recipe.name ?? ""), forKeyPath: "name")
        recipeObject.setValue((recipe.notes ?? ""), forKeyPath: "notes")
        
        for ingredient in recipe.ingredients! {
            let entityIngredient = NSEntityDescription.entity(forEntityName: "IngredientObject", in: managedContext ?? NSManagedObjectContext() )!
            let ingredientObject = NSManagedObject(entity: entityIngredient, insertInto: managedContext) as! IngredientObject
            ingredientObject.setValue((ingredient.isSelected ?? false), forKeyPath: "isSelected")
            ingredientObject.setValue((ingredient.name ?? ""), forKeyPath: "name")
            recipeObject.addToIngredients(ingredientObject)
        }
        // 4
        do {
            try managedContext?.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func fetchSavedRecipes()->[RecipeObject]{
        
        var records:[RecipeObject]!
        let managedContext = applicationDelegate?.persistentContainer.viewContext
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "RecipeObject")
        // Add Sort Descriptor
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        do {
            records = try managedContext?.fetch(fetchRequest) as! [RecipeObject]
            print(records)
           
            
//            for record in records {
//                print(record.value(forKey: "name") ?? "no name")
//            }
            //DBManager.shared.removeRecipe(recipe: records[0])
        } catch {
            print(error)
        }
        return records
    }
    
   
    
    func removeRecipe(recipe:RecipeObject){
        
        let managedContext = applicationDelegate?.persistentContainer.viewContext
        managedContext?.delete(recipe)
        do {
            try managedContext?.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
}
