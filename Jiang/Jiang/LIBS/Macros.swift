//
//  Macros.swift
//  Jiang
//
//  Created by Nidhi Singh Naruka on 15/10/20.
//  Copyright © 2020 htmchu. All rights reserved.
//

import Foundation
import UIKit



let appName = "Jiang"

let appColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
let vcBgColor = UIColor.black

let appNavBarColor = UIColor.white



let ingredients = ["Beef","BlackVinegar","Celery","ChiliOil","ChinesePickles","ChivePaste","ChoppedGarlic","Coriander","FermentedBeanCurd","FermentedSoybean","FriedSoybeans","GratedRadish","HoisinSauce","JapanesePepperOil","MushroomPaste","Peanuts","Ponzu","SachaSauce","Salt","selectedbutton","Sesame","SesameOil","SesameSauce","ShichimiSpice","SoySauce","SpicyHotpotSauce","SpringOnions","Sugar","ThaiChili"]


//Beef　牛肉 BlackVinegar　黒酢 Celery　セロリ ChiliOil　ラー油 ChinesePickles ザーサイ ChivePaste　ニラたれ ChoppedGarlic　大蒜 Coriander　パクチー FermentedBeanCurd　豆腐たれ FermentedSoybeans　とうち FriedSoybean　炒め大豆 GratedRadish　大根 HoisinSauce　海鮮たれ JapanesePepperOil　山椒油 MushroomPaste　菌たれ Peanuts　ピーナッツ Ponzu　ポン酢 SachaSauce　沙茶たれ Salt　塩 Sesame　すりごま SesameOil　ゴマ油 SesameSauce　ゴマたれ ShichimiSpice　七味 SoySauce　醤油 SpicyHotpotSauce　麻辣たれ SpringOnions　ネギ Sugar　砂糖 ThaiChili　唐辛子


let blackTransperantColor = UIColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.65)
let whiteTransperant = UIColor.init(red:255.0/255.0 , green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.65)
let screenWidth = UIScreen.main.bounds.size.width
let screenHeight = UIScreen.main.bounds.size.height


@available(iOS 13, *)
let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
let applicationDelegate = UIApplication.shared.delegate as? AppDelegate

