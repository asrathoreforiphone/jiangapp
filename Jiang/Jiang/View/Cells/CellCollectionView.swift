//
//  CellCollectionView.swift
//  Jiang
//
//  Created by Nidhi Singh Naruka on 16/10/20.
//  Copyright © 2020 htmchu. All rights reserved.
//

import UIKit

class CellCollectionView: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var titleIngrediant: UILabel!
}
