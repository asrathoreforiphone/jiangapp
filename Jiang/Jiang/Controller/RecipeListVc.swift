//
//  RecipeListVc.swift
//  Jiang

import UIKit
import CoreData

class RecipeListVc: UIViewController {
    
    @IBOutlet weak var txtFieldSearchRecipe: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    
    lazy var listDb:[RecipeObject] = DBManager.shared.fetchSavedRecipes()
    
    
    lazy var list:[Recipe] = {
        let list  = [Recipe]()

       // let list  = [Recipe(name: "Chu"),Recipe(name: "Abhi"),Recipe(name: "Htm"),Recipe(name: "Anything"),Recipe(name: "Burger")]
        return list
    }()
    
    
    
    lazy var listSearch:[RecipeObject] = {
        let list  = [RecipeObject]()
        return list
    }()
    
    
    
    var isSearching:Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(listDb)
        self.setMyNavigationBarUI(title:appName)
        
        txtFieldSearchRecipe.addTarget(self, action: #selector(searchFieldUpdate(textField:)), for: .editingChanged)
        
        tableView.register(UINib.init(nibName: "RecipeCell", bundle: nil), forCellReuseIdentifier: "RecipeCell")
        tableView.backgroundColor =  blackTransperantColor
        
        
        // tableView.register(UINib(nibName: "CustomCell", bundle: nil), forCellReuseIdentifier: "CustomCell")
        
        //  tableView.register(RecipeCell.self, forCellReuseIdentifier: "RecipeCell")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        listDb = DBManager.shared.fetchSavedRecipes()
        self.tableView.reloadData()
    }
    
}

//MARK:- UITableViewDelegate,UITableViewDataSource
extension RecipeListVc:UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return screenWidth*0.12
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return listSearch.count
        }else{
            return listDb.count
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeCell
        cell.backgroundColor = .white
        
        
        //fill the data from list in to cell
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(buttonDeleteTaped(button:)), for: .touchUpInside)
        
        if isSearching {
            cell.lblTitle.text = (self.listSearch [indexPath.row].name ?? "")
            cell.btnDelete.isHidden = true
        }else{
            cell.lblTitle.text = (self.listDb[indexPath.row].name ?? "")
            cell.btnDelete.isHidden = false
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Recipe", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "RecipeVc") as! RecipeVc
        vc.isShareMode = true
        if isSearching {
           // vc.recipe = self.listSearch[indexPath.row]
              vc.recipeObject = self.listSearch[indexPath.row]
        }else{
             //vc.recipe = self.list[indexPath.row]
             vc.recipeObject = self.listDb[indexPath.row]
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    //MARK:- UIButton Actions
    @objc func buttonDeleteTaped(button:UIButton){
        print(button.tag)
        
        //i will remove the object from the selected index
        //self.list.remove(at: button.tag)
        
        DBManager.shared.removeRecipe(recipe: self.listDb[button.tag])
        self.listDb.remove(at:button.tag )
        //then reload table view
        self.tableView.reloadData()
    }
    
    @IBAction func btnPlusREcipeTaped(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Recipe", bundle: nil)
               let vc = storyboard.instantiateViewController(withIdentifier: "RecipeVc") as! RecipeVc
               vc.isShareMode = false
               self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
}


//MARK:- UITextFieldDelegate
extension RecipeListVc:UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc func searchFieldUpdate(textField:UITextField){
         print(textField.text)
        if let searchWord = textField.text {
            if searchWord.isEmpty {
                isSearching = false
                self.listSearch = [RecipeObject]()
            }else{
                isSearching = true
                //logic for search goes here
                self.listSearch = listDb.filter({( item : RecipeObject) -> Bool in
                    return  item.name!.lowercased().contains(searchWord.lowercased())
                })
                
                
                
                
                
                
            }
            self.tableView.reloadData()
        }
    }
}




