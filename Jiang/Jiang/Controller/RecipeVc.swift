//
//  RecipeVc.swift
//  Jiang

import UIKit

class RecipeVc: UIViewController {
    
    var isShareMode:Bool = false
    //if it is coming as shrable mode
    var recipe:Recipe! = Recipe()
    
    var recipeObject:RecipeObject!

    @IBOutlet weak var txtRecipeName: UITextField!
    @IBOutlet weak var txtNotes: UITextField!
    
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isShareMode {
            self.setMyNavigationBarUI(title: (recipe.name ?? ""))
        }else{
            self.setMyNavigationBarUI(title:"Add Recipe")
        }
        
        //Simple Right Side Button we are adding on Navigation Bar ...
        let btnDoneOrShare = UIButton.init(type: .custom)
        btnDoneOrShare.addTarget(self, action: #selector(btnDoneOrShareTaped(btn:)), for: .touchUpInside)
        
    
        
        btnDoneOrShare.setTitleColor(.black, for: .normal)
        
        let barRightButton = UIBarButtonItem(customView: btnDoneOrShare)
        self.navigationItem.rightBarButtonItem = barRightButton
        
        
        
        for ingredient in ingredients {
            let ingredientObj = Ingredient.init(isSelected: false, name: ingredient, img: UIImage.init(named: ingredient ))
            recipe.ingredients?.append(ingredientObj)
        }
        
        
        if isShareMode {
                btnDoneOrShare.tag = 0
                btnDoneOrShare.setTitle("Share", for: .normal)
                txtRecipeName.text = ( recipeObject.name ?? "")
                txtNotes.text  = ( recipeObject.notes ?? "")
                txtRecipeName.isUserInteractionEnabled = false
                txtNotes.isUserInteractionEnabled = false
               // collectionView.isUserInteractionEnabled = false
            //update selected state of ingridients from Db of this perticular recipe
            let savedIngredients = (recipeObject.ingredients?.allObjects ?? [IngredientObject]()) as [IngredientObject]
            print(savedIngredients)
            for dbIngrident in savedIngredients {
                for localIng in recipe.ingredients! {
                    if dbIngrident.name == localIng.name {
                        localIng.isSelected = dbIngrident.isSelected
                    }
                }
            }
        
            }else{
                btnDoneOrShare.tag = 1
                btnDoneOrShare.setTitle("Done", for: .normal)
            }
        
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        layout.itemSize = CGSize(width: screenWidth/3, height: screenWidth/3)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        collectionView!.collectionViewLayout = layout
        collectionView!.backgroundColor = .black
    
    }
    @objc func btnDoneOrShareTaped(btn:UIButton){
        print(btn.tag)
        if btn.tag == 0 {
            //share mode
            
            
            let recipeName = (recipeObject.name ?? "")
            
            var ingredientNames =  " with selected ingredients : "
            
            for item in recipe.ingredients! {
                if item.isSelected! {
                    
                    ingredientNames = ingredientNames + " " + item.name!
                }
                
            }
            
           // print(ingredientNames)
            
            var finalString =  "Sharing the recipe : " +  recipeName + ingredientNames
            
            print(finalString)
            
            let activityVC = UIActivityViewController(activityItems: [finalString], applicationActivities: nil)
            activityVC.popoverPresentationController?.sourceView = self.view
            present(activityVC, animated: true, completion: nil)
            activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
            if completed  {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        }else{
            //Add Mode
            
            if txtRecipeName.text!.isEmpty {
                self.showAlert(message: "Kindly fill Recipe name first.")
            }else if txtNotes.text!.isEmpty {
                self.showAlert(message: "Kindly fill Notes first.")
            }else{
                recipe.name = txtRecipeName.text!
                recipe.notes = txtNotes.text!
                DBManager.shared.saveRecipe(recipe: recipe)
                self.navigationController?.popViewController(animated: true)
            }

        }
    }
}
extension RecipeVc: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.recipe.ingredients!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellCollectionView", for: indexPath) as! CellCollectionView
        if let _ingredient = self.recipe.ingredients?[indexPath.row] {
            cell.imgView.image = _ingredient.img ?? nil
            cell.titleIngrediant.text = _ingredient.name ?? ""
            if _ingredient.isSelected! {
                cell.backgroundColor = .systemPink
            }else{
                cell.backgroundColor = .clear
            }
        }
    return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if !isShareMode {
            if let _ingredient = self.recipe.ingredients?[indexPath.row] {
                       _ingredient.isSelected = !_ingredient.isSelected!
                   }
             self.collectionView.reloadData()
        }
       
    }
}
//MARK:- UITextFieldDelegate
extension RecipeVc:UITextFieldDelegate,UITextViewDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    


}
