//
//  Recipe.swift
//  Jiang
//


import Foundation
import UIKit

class Recipe {
    var name:String?
    var image:UIImage? // we are not using this TO Do
    var ingredients: [Ingredient]?
    var notes:String?
    init(name:String? = "",image:UIImage? = #imageLiteral(resourceName: "ItunesArtwork"),ingredients: [Ingredient]? = [Ingredient](),notes:String? = "Any kind of note") {
        self.name = name
        self.image = image
        self.ingredients = ingredients
        self.notes = notes
    }
}


