//
//  Ingredient.swift
//  Jiang
//


import Foundation
import UIKit

class Ingredient {
    var isSelected:Bool?
    var name:String?
    var img:UIImage?
    init(isSelected:Bool? = false ,name:String? = "Onion",img:UIImage? = #imageLiteral(resourceName: "SoySauce")) {
        self.isSelected = isSelected
        self.name = name
        self.img = img
    }

}
